package com.family.asia.demobackend.controller;

import com.family.asia.demobackend.model.Part;
import com.family.asia.demobackend.service.PartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/parts")
public class PartController {

    @Autowired
    private PartService partService;
/*
    @GetMapping("/test")
    public String test(){
        return "test";
    }
*/
    @PostMapping()
    public ResponseEntity<Part> CreatePart(@RequestBody Part part_var)
    {
        Optional<Part> part;
        //return partService.CreatePart(part_var);
        part = partService.CreatePart(part_var);
        return ResponseEntity.status(HttpStatus.OK).body(part.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Part> ModifyPart(@PathVariable("id") Integer id_var, @RequestBody Part part_var)
    {
        Optional<Part> part;
        part = partService.ModifyPart(id_var,part_var);

        if(part.isPresent())
            return ResponseEntity.status(HttpStatus.OK).body(part.get());
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @GetMapping("/{id}")
    public Optional<Part>GetOnePart(@PathVariable("id")Integer id_var)
    {
        return partService.GetOnePart(id_var);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Part> DeletePart(@PathVariable("id") Integer id_var,@RequestBody Part part_var)
    {
        Optional<Part> part;
        part = partService.DeletePart(id_var);
        if(part.isPresent())
            return ResponseEntity.status(HttpStatus.OK).body(part.get());
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @GetMapping()
    public List<Part> GetAllParts()
    {
        return partService.GetAllParts();
    }


}
