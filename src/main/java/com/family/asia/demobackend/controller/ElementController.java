package com.family.asia.demobackend.controller;

import com.family.asia.demobackend.model.Element;
import com.family.asia.demobackend.service.ElementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/elements")
public class ElementController {

    // @Value("${server.port}")
    private static Logger LOG = LoggerFactory.getLogger(ElementController.class);
    @Autowired
    private ElementService elementService;

    @GetMapping("/test")
    public String GetTest()
    {
        return "test";
    }
    /*
        @GetMapping("")
        public List<Element> GetAllElements()
        {
            LOG.info("GetAllElements");
            return service_interface.GetAllElements();
        }
    */
    @GetMapping("")
    public List<Element> GetElementsByName
    (@RequestParam(value = "elname",defaultValue = "") String xname,
     @RequestParam(value = "qfrom", defaultValue = "0") Integer qtyFrom,
     @RequestParam(value = "qto", defaultValue = "0") Integer qtyTo,
     @RequestParam Map<String, String> reqParam)
    {

        String qfrom = "qfrom";

        LOG.info("GetAllElements");
        //return service_interface.GetElementsByName(xname);
        /*
        if(xname.isEmpty() || (qtyFrom == 0 && qtyTo == 0))
        {
            return (List<Element>) (new ArrayList<Element>());
        }
        */
        if(!(reqParam.containsKey("elname")|| reqParam.containsKey("qto")&& reqParam.containsKey(qfrom)))
        {
            return (List<Element>) (new ArrayList<Element>());
        }

        if(!xname.isEmpty())
        {
            return elementService.GetElementsByName(xname);

        }
        else if (qtyFrom != 0 && qtyTo != 0)
        {
            return elementService.GetElementsUsingQuantity(qtyFrom,qtyTo);
        }
        else
        {
            return elementService.GetAllElements();
        }
    }


    @PutMapping("/{id}")
    public Element ModifyElement(@PathVariable("id") Integer id_var, @RequestBody Element el_var)
    {
        LOG.info("ModifyElement");
        return elementService.ModifyElement(id_var,el_var);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Element> GetElement(@PathVariable("id") Integer id_var)
    {
        Optional<Element> el = elementService.GetOneElement(id_var);
        if(!el.isPresent())
        {
            LOG.warn("cannot find element");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        LOG.info("successful get");
        return ResponseEntity.status(HttpStatus.OK).body(el.get());
    }

    @PostMapping()
    public Element CreateElement(@RequestBody Element el_var)
    {
        LOG.info("CreateElement");
        return (elementService.CreateElement(el_var)).get();

        /*
        Optional<Element> opt_el = service_interface.PostParticularElement(el_var);
        if(opt_el.isPresent())
        {
            return opt_el.get();
        }
        else
        {
            return null;
        }
        */
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Element> DeleteElement(@PathVariable("id")Integer var_id)
    {
        Element el_found = null;
        el_found = elementService.DeleteElement(var_id);

        if (el_found == null) {
            LOG.warn("delete error");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } else {
            LOG.warn("successful delete");
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        }
    }


}
