package com.family.asia.demobackend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.text.MessageFormat;

import static java.lang.Thread.currentThread;

//@RestController("/asia")
@RestController()
@RequestMapping("infos")
public class SimpleController {

    private static Logger LOG = LoggerFactory.getLogger(SimpleController.class);

    @GetMapping("/test")
    public String GetInfo()
    {
        LOG.info("test");
        LOG.warn("to jest próbny warn");
        return "test";
    }

    // http:localhost:8080/echo?msg=asia
    @GetMapping("/echo")
    public String GetEcho(@RequestParam(value = "msg") String param)
    {
        String s = "heellooo "+param;
        LOG.info(s);
        return s;
    }

    // http:localhost:8080/echo2?msg1=asia&msg2=rafal
    @GetMapping("/echo2")
    public String GetEcho2(@RequestParam(value = "msg1") String param, @RequestParam(value = "msg2")  String drugiparam)
    {
        String s = "heellooo "+param + " " +drugiparam;
        LOG.info(s);
        return s;
    }

    //http://localhost:8082/asia/infos/message/info123
    @GetMapping("/message/{msg}")
    public String GetPathVariable(@PathVariable("msg") String str){
        LOG.info(str);
        return str;
    }


    //http://localhost:8082/asia/infos/squaring/5
    @GetMapping("/squaring/{num:\\d+}")
    public String GetSquaredNumber(@PathVariable("num") Integer n){
        Integer result;
        result = n*n;
        LOG.info(MessageFormat.format("Squared {0} is {1}",n,result));
        return result.toString();
    }
   // http://localhost:8082/asia/infos/serverinfo
    @GetMapping("/serverinfo")
    public ServerData GetServerInfo()
    {
        ServerData sd = new ServerData();
        LOG.info(sd.toString());
        return sd;
    }

    @Value("${server.port}")
    String port;

    public class ServerData{
        public String Port;
        public Long ThreadID;

        public ServerData(){
            Port = port;
            ThreadID = currentThread().getId();
        }

        @Override
        public String toString() {
            return MessageFormat.format("Port = {0}, ThreadID = {1}",Port,ThreadID);
        }
    }


}
