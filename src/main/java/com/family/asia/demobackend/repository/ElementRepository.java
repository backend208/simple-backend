package com.family.asia.demobackend.repository;

import com.family.asia.demobackend.model.Element;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ElementRepository extends CrudRepository <Element, Integer>{

    List<Element> findAll();

    Optional<Element> findById(Integer id);

    List<Element> findByName(String name);

    List<Element> findByQuantityBetween(Integer f, Integer t);

    @Query(value = "select * from public.element where qty between ?1 and ?2",nativeQuery = true)
    List<Element> customFindByQuantityBetween(Integer from, Integer to);

    @Query(value = "select * from public.element where qty between :pfrom and :pto",nativeQuery = true)
    List<Element> customFindByQuantityBetween_v2(@Param("pfrom") Integer from, @Param("pto") Integer to);

}

