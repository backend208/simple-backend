package com.family.asia.demobackend.repository;


import com.family.asia.demobackend.model.Part;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface PartRepository extends CrudRepository<Part, Integer> {
    Optional<Part> findById(Integer id_var);

    List<Part> findByName(String name_var);

    List<Part> findAll();

   // List<Part> FindPartUsingDate(Date date_var);

   // List<Part> FindPartUsingQuantity(Integer qty_var);
}
