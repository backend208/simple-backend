package com.family.asia.demobackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Element {
    public Integer getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Integer identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Id
    @GeneratedValue
    public Integer identifier;
    @Column(length = 80, nullable = false)
    public String name;
    @Column(name = "qty")
    public Integer quantity;

    public Element() {
    }

    public Element(Integer identifier, String name, Integer quantity) {
        this.identifier = identifier;
        this.name = name;
        this.quantity = quantity;
    }
}
