package com.family.asia.demobackend.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
@Entity
public class Part {

    @Id
    @GeneratedValue
    private Integer id;
    private String validityDate;
    private String name;

    // bezparametrowy constructor
    public Part() {
    }

    //parametrowy contructor

    public Part(Integer id, String validityDate, String name) {
        this.id = id;
        this.validityDate = validityDate;
        this.name = name;
    }


    //gettery, settery


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(String validityDate) {
        this.validityDate = validityDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // obiekty klasy Part
/*
    Part p1 = new Part(1,"2022-02-02","part1");
    Part p2 = new Part(2,"2022-11-05","part2");
 */
}
