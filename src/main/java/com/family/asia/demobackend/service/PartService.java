package com.family.asia.demobackend.service;

import com.family.asia.demobackend.model.Part;

import java.util.List;
import java.util.Optional;

public interface PartService {
    Optional<Part> GetOnePart(Integer var_id);

    Optional <Part> ModifyPart(Integer var_id, Part part_var);

    Optional <Part> DeletePart(Integer var_id);

    Optional <Part> CreatePart(Part part_var);

    List<Part> GetAllParts();

}
