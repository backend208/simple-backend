package com.family.asia.demobackend.service;

import com.family.asia.demobackend.model.Part;
import com.family.asia.demobackend.repository.PartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PartServiceImpl implements PartService {

    @Autowired
    private PartRepository partRepository;

    public List<Part> partList;
    public Part p1;
    public Part p2;
    public Part p3;

    public PartServiceImpl() {
        partList = new ArrayList<Part>();
        p1 = new Part(1, "01-01-2024", "part_test1");
        p2 = new Part(2, "01-05-2025", "part_test2");
        p3 = new Part(3, "11-11-2022", "part_test3");

        partList.add(p1);
        partList.add(p2);
        partList.add(p3);
    }


    @Override
    public Optional<Part> GetOnePart(Integer var_id) {
       return partRepository.findById(var_id);

    }

    @Override
    public Optional<Part> ModifyPart(Integer var_id, Part part_var) {
      // List<Part> partListFromDb;
     //  partListFromDb = partRepository.findAll();
      Optional <Part> part_from_db;
/*
        for (Part p : partListFromDb) {
            if (var_id != p.getId()) {
                continue;
            } else {

                p.setName(part_var.getName());
                p.setValidityDate(part_var.getValidityDate());
                return Optional.of(partRepository.save(p));
            }
        }
*/
        part_from_db = partRepository.findById(var_id);
        if(!part_from_db.isPresent())
        {
            return Optional.empty();
        }
        else
        {
            Part part_to_save;
            part_to_save = part_from_db.get();
            part_to_save.setName(part_var.getName());
            part_to_save.setValidityDate(part_var.getValidityDate());
            Part returned_part;
            returned_part = partRepository.save(part_to_save);
            return Optional.of(returned_part);

          //  return Optional.of(partRepository.save(part_from_db.get()));
        }

    }

    @Override
    public Optional<Part> DeletePart(Integer var_id) {
        Optional<Part> part_from_db;
        part_from_db = partRepository.findById(var_id);
        if (!part_from_db.isPresent()) {
            return Optional.empty();
        } else {
            partRepository.deleteById(var_id);
            return part_from_db;
        }
    }
    @Override
    public Optional<Part> CreatePart(Part part_var) {
        return Optional.of(partRepository.save(part_var));
    }

    @Override
    public List<Part> GetAllParts() {
        return partRepository.findAll();
    }
}
