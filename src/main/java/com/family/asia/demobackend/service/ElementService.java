package com.family.asia.demobackend.service;

import com.family.asia.demobackend.model.Element;

import java.util.List;
import java.util.Optional;

public interface ElementService {

    public Optional<Element> GetOneElement(Integer identifier);
    public List<Element> GetAllElements ();

    public List<Element> GetElementsByName (String name);
    public Element ModifyElement(Integer identifier, Element el_to_change);
    public Element DeleteElement(Integer identifier);
    public Optional<Element> CreateElement(Element element);

    public List<Element> GetElementsUsingQuantity (Integer qtyfrom, Integer qtyto);

    // public Element GetParticularElement2(Integer identifier);

}
