package com.family.asia.demobackend.service;

import com.family.asia.demobackend.model.Element;
import com.family.asia.demobackend.repository.ElementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ElementServiceImpl implements ElementService {

    private static Logger LOG = LoggerFactory.getLogger(ElementServiceImpl.class);

    @Autowired
    private ElementRepository elementRepository;

    public List<Element>listTestObjects;
    public Element testObject;
    public Element testObject2;
    public Element testObject3;


    public ElementServiceImpl() {
        Element testObject = new Element(1,"test1",1);
        Element testObject2 = new Element(2,"test2",2);
        Element testObject3 = new Element(3,"test3",3);
        listTestObjects = new ArrayList<Element>();
        listTestObjects.add(testObject);
        listTestObjects.add(testObject2);
        listTestObjects.add(testObject3);

        //return listTestObjects;
    }

    @Override
    public Optional<Element> GetOneElement(Integer identifier) {

        /*
        for (Element element: listTestObjects) {
            if (identifier == element.getIdentifier()) {
                return Optional.of(element);
            } else {
                continue;
            }
        }
        return Optional.empty();


   */

        return elementRepository.findById(identifier);
    }
    // override druga wersja getparticularelement
/*
    @Override
public Element GetParticularElement2(Integer identifier) {

    if (identifier == testObject.identifier)
        {
            return testObject;
        }
    else if (identifier == testObject2.identifier)
    {
        return testObject2;
    }
    else if (identifier == testObject3.identifier)
    {
        return testObject3;
    }

    else
    {
        return null;
    }
}
*/
//Put
    public Element ModifyElement(Integer identifier, Element el_to_change)
    {
        for (Element element : listTestObjects)
        {
            if(identifier == element.getIdentifier())
            {
                element.setName(el_to_change.getName());
                element.setQuantity(el_to_change.getQuantity());
                return element;
            }
            else
            {
                continue;
            }
        }
        return null;
    }
//Post

    public Optional<Element> CreateElement(Element element)
    {
        /*
        listTestObjects.add(element);
        return Optional.of(element);
        */
        return Optional.of(elementRepository.save(element));
        //return Optional.of(element);
    }

    @Override
    public List<Element> GetElementsUsingQuantity(Integer qtyFrom, Integer qtyTo) {
        List<Element> emptyList = new ArrayList<Element>();
        if(qtyFrom > qtyTo)
        {
            LOG.info("Wartosc from jest wieksza od wartosci to");
            return emptyList;
        }
        else
        {
            LOG.info("Realizacja zapytania z zakresem na Quantity");
            // return elementRepository.findByQuantityBetween(qtyFrom,qtyTo);
            // return elementRepository.customFindByQuantityBetween(qtyFrom,qtyTo);
            return elementRepository.customFindByQuantityBetween_v2(qtyFrom,qtyTo);
        }
    }

//Delete

    @Override
    public List<Element> GetAllElements() {
      /*
       if(listTestObjects != null)
        {
            return listTestObjects;
        }
       else
           return null;
       */

        return elementRepository.findAll();
    }

    @Override
    public List<Element> GetElementsByName(String name) {
        return elementRepository.findByName(name);
    }


    @Override
    public Element DeleteElement(Integer identifier) {
        Element el_found = null;
        for (Element el : listTestObjects) {
            if (el.getIdentifier() == identifier) {
                el_found = el;
                break;
            }
        }
        if(el_found == null)
        {
            return null;
        }
        else
        {
            listTestObjects.remove(el_found);
            return el_found;
        }


    }


}
