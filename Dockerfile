# file:dockerfile

FROM openjdk:18

COPY ./build/libs/app.jar /app/

ENTRYPOINT java -jar /app/app.jar  --server.port=8988

EXPOSE 8988